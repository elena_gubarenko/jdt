// import { Suspense } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import routes from "./Routes/routes"
import Container from "./Components/Container"
import Homepage from './Components/Homepage'
import AddProductPage from './Components/AddProductPage'
import Footer from './Components/Footer'

function App() {
  return (
    <>
      <Container>
         <BrowserRouter>        
            <Switch>
              <Route path={routes.homepage} exact component={Homepage} />
              <Route path={routes.addProductPage} component={AddProductPage}/>
            </Switch> 
        </BrowserRouter>
        <Footer/>
     </Container>
    </>
  );
}

export default App;
