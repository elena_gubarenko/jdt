import styles from './AddProductPage.module.css'
import { useState, useEffect } from 'react';
import { useSelector, useDispatch } from "react-redux"
import operations from '../../Redux/operations/operations';
import routes from '../../Routes/routes'
import { NavLink } from 'react-router-dom';
import selectors from '../../Redux/selectors/selectors'

let productCardId = -1

export default function AddProductPage() {
  const products = useSelector(selectors.products)
  const [selectValue, setSelectValue] = useState('Type Switcher')
  const [skuValue, setSkuValue] = useState('')
   const [nameValue , setNameValue ] = useState('')
  const [priceValue, setPriceValue] = useState('')
  const [dvdSizeValue, setDvdSizeValue] = useState('')
  const [bookWeightValue, setBookWeightValue] = useState('')
  const [furnitureHeightValue, setFurnitureHeightValue] = useState('')
  const [furnitureWidthValue, setFurnitureWidthValue] = useState('')
  const [furnitureLengthValue, setFurnitureLengthValue] = useState('')
  const dispatch = useDispatch()
  const [productExist, setProductExist] = useState(false)
  const [validationOk, setValidationOk] = useState(false)
  const [errorMessage,  setErrorMessage] = useState('')

 
  const checkInput = ([...values]) => {   
    values.map(value => {
      console.dir(value)
      if (value === '' || value === ' ' || selectValue=== 'Type Switcher') {
        return setValidationOk(false)
      }
      if (value !== '' || value !== ' ' || selectValue!== 'Type Switcher') {
        return setValidationOk(true)
}    })
    }

  const handleSelect = (e) => {
    setSelectValue(e.target.value)
  }
  
  const handleSku = (e) => {
         setSkuValue(e.target.value)

  }
    
  const handleName = (e) => {
    setNameValue(e.target.value)
  }

    const handlePrice = (e) => {
        setPriceValue(e.target.value)
  }

  const handleDvdSize = (e) => {
         setDvdSizeValue(e.target.value)
  }

  const handleBookWeight = (e) => {
             setBookWeightValue(e.target.value)
  }

  const handleFurnitureHeight = (e) => {
              setFurnitureHeightValue(e.target.value)
  }

  const handleFurnitureWidth = (e) => {
            setFurnitureWidthValue(e.target.value)
  }

  const handleFurnitureLength = (e) => {
             setFurnitureLengthValue(e.target.value)
  }

  const checkProduct = () => {
    const isProductExist = products.filter(product => {   
      return product.skuValue ===  skuValue       
    })
    
    if (isProductExist.length !== 0) {
      setProductExist(true)
        alert('exist') 
    }

     if (isProductExist.length === 0) {
      setProductExist(false)
    }
  }

  useEffect(() => {
    checkProduct()
  }, [skuValue])

  useEffect(() => {
    checkInput([skuValue, nameValue, priceValue, dvdSizeValue])
  },[skuValue, nameValue, priceValue, dvdSizeValue])

   useEffect(() => {
    checkInput([skuValue, nameValue, priceValue, bookWeightValue])
   }, [skuValue, nameValue, priceValue, bookWeightValue])
  
   useEffect(() => {
    checkInput([skuValue, nameValue, priceValue, furnitureHeightValue, furnitureWidthValue, furnitureLengthValue])
  },[skuValue, nameValue, priceValue, furnitureHeightValue, furnitureWidthValue, furnitureLengthValue])
  

  const addProduct = () => {
     productCardId +=1
    let productCard

    if (selectValue === 'DVD' && !productExist && validationOk) {
            
      productCard = { productCardId, category: selectValue, skuValue, nameValue, priceValue, dvdSizeValue }
      
      dispatch(operations.addProductCard(productCard))
      

    }

    if (selectValue === 'Book' && !productExist && validationOk) {
      productCard = { productCardId, category: selectValue, skuValue, nameValue, priceValue, bookWeightValue }
      
      dispatch(operations.addProductCard(productCard))
    }

    if (selectValue === 'Furniture' && !productExist &&validationOk) {
      productCard = { productCardId, category: selectValue, skuValue, nameValue, priceValue, furnitureHeightValue, furnitureWidthValue, furnitureLengthValue }
      
      dispatch(operations.addProductCard(productCard))
    }

    if (productExist) { alert('exist') }
  }

  const isFilled = (value, inputId) => {
    if (value === '' || value === ' ') {
     return setErrorMessage(`Please, submit required data: ${inputId}`)
    }
    if (value !== '' || value !== ' ') {
     return setErrorMessage('')
    }
  }

  return (
    <>
      <div className={styles.Header}> 
        <p className={styles.ErrorMessage}>{errorMessage}</p>
        <h2 className={styles.Title}>Product Add</h2>
<div className={styles.ButtonDiv}>
        <NavLink className={styles.AddProduct} onClick={addProduct}
          to={
              !productExist && validationOk ? routes.homepage : routes.addProductPage}
            
        >Save</NavLink>  
         <NavLink 
          to={  routes.homepage}>Cancel</NavLink>  </div> 
      </div>

      <form className={styles.Form} id='product_form'>
        <label className={styles.Label}>SKU
          <input
            className={styles.Input}
            required
            onChange={handleSku}
            value={skuValue}
            onBlur={()=>isFilled(skuValue, 'sku')}
            title='Please, submit required data'
            type='text'
            id='sku'></input>
        </label>
        <label className={styles.Label}>Name
          <input
             className={styles.Input}
            onChange={handleName}
            value={nameValue}
            onBlur={()=>isFilled(nameValue, 'name')}
            title='Please, submit required data'
            type='text'
            required
            id='name'></input>
        </label>
        <label className={styles.Label}>Price ($)
          <input
             className={styles.Input}
            onChange={handlePrice}
            value={priceValue}
             onBlur={()=>isFilled(priceValue, 'price')}
            title='Please, submit required data'
            type='number'
            required id='price'></input>
        </label>
        <label className={styles.Label}>Type Switcher
          <select
             className={styles.Input}
           title='Please, submit required data'
            required value={selectValue} onChange={handleSelect} id='productType'>
            <option disabled selected value='Type Switcher'>Type Switcher</option> 
            <option id='DVD' value='DVD'>DVD</option>
            <option id='Book' value='Book'>Book</option>
            <option id='Furniture' value='Furniture'>Furniture</option>
          </select>
        </label>
        {selectValue === 'DVD' ? 
         <>
          <label className={styles.Label}>Size (MB)
              <input   
                 className={styles.Input}
                onChange={handleDvdSize}  
                value={dvdSizeValue}
                onBlur={()=>isFilled(dvdSizeValue, 'size')}
                title='Please, provide the data of indicated type'
                type='number'
                required id='size'></input>
            </label>
          <p>"Please, provide dvd size in MB format"</p>  
</>
          : null
        }
        {selectValue === 'Book' ? 
          <>
            <label className={styles.Label}>Weight (KG)
              <input
                 className={styles.Input}
                onChange={handleBookWeight}
                value={bookWeightValue}
                onBlur={()=>isFilled(bookWeightValue, 'weight')}
                title='Please, provide the data of indicated type'
                 type='number'
                required id='weight'></input>
          </label> 
             <p>"Please, provide book weight in KG format"</p>
             </>
          : null
        }
            {selectValue === 'Furniture' ? 
         <>
         <label className={styles.Label}>Height (CM)
              <input
                 className={styles.Input}
                onChange={handleFurnitureHeight}
                value={furnitureHeightValue}
                onBlur={()=>isFilled(furnitureHeightValue, 'height')}
                title='Please, provide the data of indicated type'
                 type='number'
                required id='height'></input>
          </label> 
           <label className={styles.Label}>Width (CM)
              <input
                 className={styles.Input}
                onChange={handleFurnitureWidth}
                value={furnitureWidthValue}
                  onBlur={()=>isFilled(furnitureWidthValue, 'width')}
                title='Please, provide the data of indicated type'
                 type='number'
                required id='width'></input>
          </label> 
           <label className={styles.Label}>Length (CM)
              <input
                 className={styles.Input}
                onChange={handleFurnitureLength}
                value={furnitureLengthValue}
                  onBlur={()=>isFilled(furnitureLengthValue, 'length')}
                title='Please, provide the data of indicated type'
                 type='number'
                required id='length'></input>
            </label>
            <p>"Please, provide dimensions in HxWxL format"</p>
          </>
          : null
      }     
      </form>
   </>   
  )
}