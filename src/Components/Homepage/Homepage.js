import styles from './Homepage.module.css'
import { NavLink } from 'react-router-dom';
import routes from '../../Routes/routes'
import selectors from '../../Redux/selectors/selectors'
import { useSelector, useDispatch } from 'react-redux';
import { useState } from "react"
import operations from '../../Redux/operations/operations';

export default function Homepage() {
  const products = useSelector(selectors.products)
  const [checkboxesToDelete, setCheckboxesToDelete] = useState([])
  const dispatch = useDispatch()
  
  const handleCheckbox = (e) => {
    // console.dir(e.target)

    if (e.target.checked) {
  setCheckboxesToDelete((prevState) => {
            return [...prevState, e.target.id]
    })
}
    if (!e.target.checked) {
      setCheckboxesToDelete((prevState) => {
        return prevState.filter((element) => {
        return element!==e.target.id
      })
          
    })
    }
  }

  const massDelete = () => {
    dispatch(operations.massDelete(checkboxesToDelete))
  }
    
  return (
    <><div className={styles.Header}>
      <h2 className={styles.Title}>Product List</h2>
      <div className={styles.ButtonDiv}>
        <NavLink className={styles.Add} to={routes.addProductPage}>ADD</NavLink>
      <button className={styles.ButtonDelete} onClick={massDelete} type='button' id='delete-product-btn'>MASS DELETE</button>
      </div>
    </div>
      <div className={styles.ProductsList}> 
      {
        products.map(product => {
          return  <div className={styles.ProductCard}>
            <input  onChange={handleCheckbox} id={product.productCardId}
              className={`delete-checkbox ${styles.Checkbox}`} type='checkbox'></input>
          <ul className={styles.CardUl}>
                      <li>{ product.skuValue}</li>
            <li>{ product.nameValue}</li>
            <li>{product.priceValue} $</li> 
            {product.category === 'DVD' ?
              <li>Size: { product.dvdSizeValue}</li> :null
            } 
             {product.category === 'Book' ?
              <li>Weight: { product.bookWeightValue}</li> :null
            } 
            {product.category === 'Furniture' ?
              <li>Dimension: {`${product.furnitureLengthValue}x${product.furnitureWidthValue}x${product.furnitureHeightValue}`}</li> :null
            } 
            </ul>
            </div>
           
        
        } 
          ) 
      
}
       </div>
    </>


  )
}