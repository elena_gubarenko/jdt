import actions from '../actions/actions'

const addProductCard = (card) => (dispatch) => {
  dispatch(actions.addProductCard(card))
}

const massDelete = (arrOfIdToDelete) => (dispatch) => {
  dispatch(actions.massDelete(arrOfIdToDelete))
}

export default {
  addProductCard,
  massDelete
}