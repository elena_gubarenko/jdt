import { createAction } from "@reduxjs/toolkit"

const addProductCard = createAction('addProductCard')

const massDelete = createAction('massDelete')

export default {
  addProductCard,
  massDelete
}